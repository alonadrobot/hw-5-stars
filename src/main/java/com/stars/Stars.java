package com.stars;

public class Stars {
    public static void main(String[] args) {
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                System.out.print(" *");
            }
            System.out.println("");
        }
        System.out.println("===============");
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                if (i == 0 || i == 6) {
                    System.out.print(" *");
                }else {
                    if(j==0||j==6){
                        System.out.print(" *");
                    }else {
                        System.out.print("  ");
                    }
                }
            }
            System.out.println("");
        }
        System.out.println("===============");
        for (int i = 0; i < 7; i++) {
            for (int j = 7-i; j > 0; j--) {
                if (i == 0 || j == 1) {
                    System.out.print(" *");
                } else if (j == 7 - i) {
                    System.out.print(" *");
                } else {
                    System.out.print("  ");
                }
            }

            System.out.println("");
        }
        System.out.println("===============");
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                if (i == 0||j==6) {
                    System.out.print(" *");
                } else if(j==i){
                    System.out.print(" *");
                } else {
                    System.out.print("  ");
                }
            }

            System.out.println("");
        }
        System.out.println("===============");
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                if (i == 6||j==0) {
                    System.out.print(" *");
                } else if(j==i){
                    System.out.print(" *");
                } else {
                    System.out.print("  ");
                }
            }

            System.out.println("");
        }
        System.out.println("===============");
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                if (i == 6||j==6) {
                    System.out.print(" *");
                } else if(j==6-i){
                    System.out.print(" *");
                } else {
                    System.out.print("  ");
                }
            }

            System.out.println("");
        }
        System.out.println("===============");
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {

                if(j==6-i){
                    System.out.print(" *");
                } else if(j==i) {
                    System.out.print(" *");
                }else{
                    System.out.print("  ");
                }
            }

            System.out.println("");
        }
        System.out.println("===============");
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                if (i < 7 / 2+1) {
                    if (i == 0) {
                        System.out.print(" *");
                    } else if (j == 6 - i) {
                        System.out.print(" *");
                    } else if (j == i) {
                        System.out.print(" *");
                    } else {
                        System.out.print("  ");
                    }
                }
            }
            System.out.println("");
        }
        System.out.println("===============");
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                if (i > 7 / 2-1) {
                    if (i == 6) {
                        System.out.print(" *");
                    } else if (j == 6 - i) {
                        System.out.print(" *");
                    } else if (j == i) {
                        System.out.print(" *");
                    } else {
                        System.out.print("  ");
                    }
                }
            }
            System.out.println("");
        }
    }
}